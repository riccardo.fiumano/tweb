<?php include("top.html"); ?>
    <form action="signup-submit.php" method="post">
        <fieldset>
            <legend>New User Signup:</legend>
            <label class="left">Name: </label>
            <input type="text" name="name" maxlength="16" pattern="[A-Za-z ]+" title="Non inserire caratteri speciali"
                   required>
            <br>
            <label class="left">Gender: </label>
            <input type="radio" name="gender" value="M" required>Male
            <input type="radio" name="gender" required
                   value="F" checked="checked">Female
            <br>
            <label class="left">Age: </label>
            <input type="text" name="age" size="6" maxlength="2" required pattern="[0-9]+"
                   title="Non inserire caratteri speciali">
            <br>
            <label class="left">Personality type: </label>
            <input type="text" name="personality" size="6" maxlength="4" required pattern="[A-Za-z]+"
                   title="Non inserire caratteri speciali"> (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp" target="_blank">Don't
                know your type?</a>)
            <br>
            <label class="left">Favorite OS: </label>
            <select name="favorite">
                <option>Windows</option>
                <option>Mac OS X</option>
                <option selected>Linux</option>
            </select>
            <br>
            <label class="left">Seeking age: </label>
            <input type="text" name="seeking1" maxlength="2" size="6" placeholder="min" required pattern="[0-9]+"
                   title="Non inserire caratteri speciali"> to
            <input type="text" name="seeking2" size="6" maxlength="2"
                   placeholder="max" required pattern="[0-9]+"
                   title="Non inserire caratteri speciali">

            <br>
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
<?php include("bottom.html"); ?>