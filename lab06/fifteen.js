// 15 real tiles
// 1 empy tile in the lower right corner
//
//     0   100  200  300
//   0 +----+----+----+----+
//     |    |    |    |    |
// 100 +----+----+----+----+
//     |    |    |    |    |
// 200 +----+----+----+----+
//     |    |    |    |    |
// 300 +----+----+----+----+
//     |    |    |    |empty
//     +----+----+----+

// -----------------
// --- CONSTANTS ---
// -----------------

const TILE_DIM = 100; // dimension of a tile

const EMPTY_TILE_START_POS_X = (TILE_DIM * 3); // starting x position of the empty tile
const EMPTY_TILE_START_POS_Y = (TILE_DIM * 3); // starting y position of the empty tile

// ------------------------
// --- GLOBAL VARIABLES ---
// ------------------------

var emptyTilePosX = EMPTY_TILE_START_POS_X;	// x position of the empty tile (while playing)
var emptyTilePosY = EMPTY_TILE_START_POS_Y;	// y position of the empty tile (while playing)
var puzzleTiles = []; // puzzle with all tiles

// --------------
// --- ONLOAD ---
// --------------

window.onload = function() {
    // create the puzzle
    CreatePuzzle();

    // add click observer of the suffle botton
    $("shufflebutton").observe("click", ShuffleClick);
}

// -----------------------
// --- EVENT FUNCTIONS ---
// -----------------------

function ShuffleClick() {
    // random number of moves (between 20 and 200)
    var	randomMoves = Math.floor((Math.random() * 200) + 20);

    for(var i = 0; i < randomMoves; i++) {
        // array of tiles that can be moved
        var movableTiles = [];

        // check the "movability" of each tile
        for(var p = 0; p < puzzleTiles.length; p++) {
            if(CheckCloseEmptyTile(puzzleTiles[p].style.left, puzzleTiles[p].style.top)) {
                movableTiles.push(puzzleTiles[p]);
            }
        }

        // move a tile at random among the ones that can be moved
        MoveTile(movableTiles[Math.floor(Math.random() * movableTiles.length)]);
    }
}

function TileClick(event) {
    // move the tile
    if(MoveTile(this)) {
        // if the puzzle is completed, show a message
        if(CheckPuzzle()) {
            alert("CONGRATS! You solved the puzzle!");
        }
    }
}

function TileMouseOver(event) {
    // highlight the tile if the tile can be moved
    if(CheckCloseEmptyTile(this.style.left, this.style.top)) {
        this.addClassName("tile_ok");
        // de-highlight the tile if the tile is already highlighted
    } else if(this.hasClassName("tile_ok")) {
        this.removeClassName("tile_ok");
    }
}

// -----------------
// --- FUNCTIONS ---
// -----------------

function CreatePuzzle() {
    var y = 0;
    var offset = 3;

    // get the div tiles
    puzzleTiles = $$('#puzzlearea div');

    // create and position the tiles
    for(var i = 0; i < puzzleTiles.length; i++) {
        // get the position in the grid
        x = i % 4;
        y = Math.floor(i / 4);

        // add all attributes
        puzzleTiles[i].addClassName("tile");
        puzzleTiles[i].style.left = TILE_DIM * x  + "px";
        puzzleTiles[i].style.top = TILE_DIM * y + "px";
        puzzleTiles[i].style.backgroundPosition = -x * TILE_DIM + "px " + y * -TILE_DIM + "px";
        puzzleTiles[i].observe("click", TileClick);
        puzzleTiles[i].observe("mouseover", TileMouseOver);
    }
}

function MoveTile(tile) {
    // if the tile can be moved
    if(CheckCloseEmptyTile(tile.style.left, tile.style.top)) {
        // get its position
        var x = parseInt(tile.style.left);
        var y = parseInt(tile.style.top);

        // switch the position with the empty tile
        tile.style.left = emptyTilePosX + "px";
        tile.style.top = emptyTilePosY + "px";

        emptyTilePosX = x;
        emptyTilePosY = y;

        return(true);
    }
    return(false);
}

function CheckCloseEmptyTile(x, y) {
    var x = parseInt(x);
    var y = parseInt(y);

    // if the current tile has close the empty tile, then return true
    if(Math.abs(emptyTilePosY - y) == TILE_DIM && Math.abs(emptyTilePosX - x) == 0
        || Math.abs(emptyTilePosX - x) == TILE_DIM && Math.abs(emptyTilePosY - y) == 0) {
        return(true);
    }
    // return false otherwise
    return(false);
}

function CheckPuzzle() {
    // check if the empty tile is at the starting position
    if(emptyTilePosX != EMPTY_TILE_START_POS_X || emptyTilePosY != EMPTY_TILE_START_POS_Y) {
        return(false);
    }

    // iterate all tiles
    for(var i = 0; i < puzzleTiles.length; i++) {
        // get the position in the grid
        x = i % 4;
        y = Math.floor(i / 4);

        // return false if it is in the wrong position
        if(x * TILE_DIM != parseInt(puzzleTiles[i].style.left) || y * TILE_DIM != parseInt(puzzleTiles[i].style.top)) {
            return(false);
        }
    }

    // return true if all tiles are in the correct position
    return(true);
}
