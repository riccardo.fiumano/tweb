<?php
$movie = $_GET["film"];
$info = file_get_contents($movie . '/info.txt');
$info = explode(PHP_EOL, $info);
$overview = file_get_contents($movie . '/overview.txt');
$overview = explode(PHP_EOL, $overview);

function banner($info)
{
    if ($info[2] > 60) {
        echo '<img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/freshbig.png" alt="Rotten">';
    } else {
        echo '<img src="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rottenbig.png" alt="Rotten">';
    }
}

function bannerino($gradimento)
{
    if ($gradimento == 'ROTTEN') {
        return 'http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rotten.gif';
    } else {
        return 'https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif';
    }
}

$reviews_path = glob($movie.'/review*.txt');
$reviews = [];
for ($i=0;$i<10;$i++) {
    $review = file_get_contents($reviews_path[$i]);
    if($review!=false) {
        $review = explode(PHP_EOL,$review);
        array_push($reviews,$review);
    }
}


?>


<!DOCTYPE html>

<head>
    <link rel="shortcut icon" type="image/png"
          href="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif"/>
    <title>TMNT - Rancid Tomatoes</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="movie.css" type="text/css" rel="stylesheet">
</head>

<body>
<div class="banner">
    <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
</div>

<h1 id="titolo"><?php echo $info[0] . " (" . $info[1] . ")"; ?></h1>

<div class="bordo">
    <div class="spe">
        <img src="<?php echo $movie . '/overview.png' ?>" alt="general overview">
        <dl>
            <?php
            foreach ($overview as $line) {
                $line = explode(':', $line);
                echo '<dt>' . $line[0] . '</dt>';
                echo '<dd>' . $line[1] . '</dd>';
            }
            ?>
        </dl>
    </div>
    <div id="left-side">
        <div class="banner2">
            <?php banner($info);
            echo $info[2] . '%' ?>
        </div>
        <div class="colsx">
            <?php
            if (count($reviews) % 2 == 0) {
                $mid = count($reviews) / 2;
            } else {
                $mid = round(count($reviews) / 2);
            }
            for ($i = 0; $i < $mid; $i++) {
                echo '<div class="y">
                        <p>
                            <img src="' . bannerino($reviews[$i][1]) . '" alt="Rotten">
                            <q>' . $reviews[$i][0] . '</q>
                        </p>
                    </div>
                    <div class="j">
                        <p>
                            <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif">
                            ' . $reviews[$i][2] . ' <br>
                            <span class="ita">' . $reviews[$i][3] . '</span>
                        </p>
                    </div>';
            }
            ?>
        </div>
        <div class="coldx">
            <?php
            for ($i = $mid; $i < count($reviews); $i++) {
                echo '<div class="y">
                        <p>
                            <img src="' . bannerino($reviews[$i][1]) . '" alt="Rotten">
                            <q>' . $reviews[$i][0] . '</q>
                        </p>
                    </div>
                    <div class="j">
                        <p>
                            <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif">
                            ' . $reviews[$i][2] . ' <br>
                            <span class="ita">' . $reviews[$i][3] . '</span>
                        </p>
                    </div>';
            }
            ?>
        </div>

    </div>
    <div id="footer">
        <p>(1-<?php echo count($reviews) . ')' . PHP_EOL . 'of' . PHP_EOL . count($reviews) ?></p>
    </div>


</div>

<div id="vali">
    <a href="ttp://validator.w3.org/check/referer"><img
                src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png"
                alt="Validate HTML"></a> <br>
    <a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss"
                                                                    alt="Valid CSS!"></a>
</div>
</body>
</html>
