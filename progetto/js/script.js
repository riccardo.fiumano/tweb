$('#coilMath').submit(function (event) {
    event.preventDefault();

    let lunghezza = document.getElementsByName('lunghezza')[0].value;
    let materiale = document.getElementsByName('materiale')[0].value;
    let ohm = document.getElementsByName('ohm')[0].value;

    //console.log(lunghezza,materiale,ohm);

    if (materiale !=='default') {
        if(lunghezza === '' && ohm==='') {
            alert('Il campo lunghezza e OHM sono entrambi vuoti, usare almeno 1 dei due');
            //console.log('Materiale e ohn sono vuoti, usare almeno 1 dei due')
        } else {
            let data = materiale.split('-');
            //console.log(data);
            let risultato;
            if (lunghezza !=='') {
                //console.log("Formula per materiale e lunghezza");
                risultato = Number(data[1] * (lunghezza/data[0]) / 10000).toFixed(3);
            } else {
                risultato = Number(ohm * 10000 / data[1] * data[0]).toFixed(3);
                //console.log("Formula per lunghezza e ohm");
            }

            //console.log('Il risultato è ' + risultato);
            $('#risultato').val(risultato);

            if(document.getElementsByName('pref')[0].checked !== 'undefined' && document.getElementsByName('pref')[0].checked === true) {
                let form = $(event.target);
                $.post( form.attr("action"), form.serialize(),function (res) {
                    //console.log(res);
                    let j = JSON.parse(res);

                    if(j.status === 'fail') {
                        alert(j.messaggio);
                    } else {
                        alert('Preferito aggiunto!');
                        window.location.href = '/';
                    }
                })
            }
        }
    } else {
        alert('Il campo lunghezza non può essere vuoto');
        //console.log('Lunghezza vuoto');
    }
});

$('#reg').submit(function (event) {
    event.preventDefault();

    let username = document.getElementsByName('username')[0].value;
    let password = document.getElementsByName('password')[0].value;

    if (username === '' || password ==='') {
        alert('Compilare entrambi i campi');
    } else {
        let form = $(event.target);
        $.post( form.attr("action"), form.serialize(),function (res) {
            //console.log(res);
            let j = JSON.parse(res);
            if(j.status === 'fail') {
                alert(j.messaggio);
            } else {
                alert('registrazione avvenuta con successo!');
                window.location.href = '/login.php';
            }
        })
    }
});

$('#login').submit(function (event) {
    event.preventDefault();

    let username = document.getElementsByName('username')[0].value;
    let password = document.getElementsByName('password')[0].value;

    if (username === '' || password ==='') {
        alert('Compilare entrambi i campi');
    } else {
        let form = $(event.target);
        $.post( form.attr("action"), form.serialize(),function (res) {
            //console.log(res);
            let j = JSON.parse(res);
            if(j.status === 'fail') {
                alert(j.messaggio);
            } else {
                alert('Accesso avvenuta con successo!');
                window.location.href = '/';
            }
        })
    }
});

$('.trash').click(function () {
    let row = $(this).parent().parent();
    let id = row.find(">:first-child")[0].innerHTML;
    //console.log(id);
    $.post({
        type: 'POST',
        url: '/rest.php',
        data: {
            fav_rm: true,
            id: id
        },
        success: function (res) {
            //console.log(res);
            if (res.status === 'fail') {
                alert(res.messaggio);
            } else {
                row.slideUp();
            }
        }
    })
});

$('#logout').click(function () {
   $.post({
       type: 'POST',
       url: 'rest.php',
       data: {
           logout: true
       },
       success: function () {
           location.reload();
       }
   })
});