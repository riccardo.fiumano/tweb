<?php
/**
 * Created by PhpStorm.
 * User: riccardo
 * Date: 07/02/2019
 * Time: 16:49
 */

require 'include/header.php';
?>
    <h1>Accedi per continuare</h1>
    <form id="login" action="http://localhost:8888/rest.php">
        <img id="coil" src="../img/coil.jpg">
        <table>
            <tr>
                <td>
                    <img src="../img/icon/user.svg">
                </td>
                <td>
                    <input type="text" name="username" placeholder="Inserisci Username">
                </td>
            </tr>
            <tr>
                <td>
                    <img src="../img/icon/lock.svg">
                </td>
                <td>
                    <input type="password" name="password" placeholder="Inserisci la Password">
                </td>
            </tr>
        </table>
        <input id="reset" type="reset">
        <input id="submit" type="submit" name="submit">
    </form>
<?php
require 'include/footer.php';
