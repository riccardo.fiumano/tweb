<?php
require 'include/App.php';
session_start();
if(isset($_POST['logout'])) {
    session_destroy();
}
switch ($_SERVER['HTTP_REFERER']) {
    case 'http://localhost:8888/registrazione.php':
        if (isset($_POST['username'], $_POST['password'])) {
            App::register($_POST['username'], $_POST['password']);
        } else {
            echo '{"status":"fail","messaggio":"Mancano parametri!"}';
        }
        break;
    case 'http://localhost:8888/login.php':
        if (isset($_POST['username'], $_POST['password'])) {
            App::login($_POST['username'], $_POST['password']);
        } else {
            echo '{"status":"fail","messaggio":"Mancano parametri!"}';
        }
        break;
    case 'http://localhost:8888/':
        if (isset($_POST['fav_rm'], $_POST['id'])) {
            App::rmCoil($_POST['id']);
        } else if (isset($_SESSION['user_id'], $_POST['materiale'], $_POST['lunghezza'], $_POST['risultato'])) {
            App::addCoil($_SESSION['user_id'], $_POST['materiale'], $_POST['lunghezza'], $_POST['risultato']);
        } else {
            echo '{"status":"fail","messaggio":"Mancano parametri!"}';
        }
        break;
    default:
        break;
}