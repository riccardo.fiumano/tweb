<?php
/**
 * Created by PhpStorm.
 * User: riccardo
 * Date: 07/01/2019
 * Time: 16:25
 */
require 'include/header.php';
?>
    <h1>Calcola subito la tua resistenza!</h1>
    <form id="coilMath" action="rest.php" method="post">
        <img src="../img/coil.jpg">
        <table>
            <tr>
                <td>Lunghezza del filo</td>
                <td>
                    <input type="text" name="lunghezza">
                </td>
            </tr>
            <tr>
                <td>Materiale</td>
                <td>
                    <select name="materiale">
                        <option value="default" disabled selected hidden>--Scegli un filo--</option>
                        <?php App::dropDown(); ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Valore in OHM che vuoi ottenere</td>
                <td>
                    <input type="text" name="ohm">
                </td>
            </tr>
            <br>
            <?php
            if(isset($_SESSION['is_logged'])) {
                echo '            <tr>
                <td>Salva nei preferiti</td>
                <td>
                    <input type="checkbox" name="pref">
                </td>
            </tr>';
            }
            ?>
            <br>
            <tr>
                <td>Risultato</td>
                <td><input type="text" id="risultato" name="risultato" readonly></td>
            </tr>
            <tr>
                <td>
                    <input id="reset" type="reset">
                    </td>
                <td>
                    <input type="submit" name="submit">
                </td>
            </tr>
        </table>
    </form>
    <form id="preferiti">
    <table>
        <caption><img id="pref" src="../img/icon/award.svg"></caption>
        <?php
            if(isset($_SESSION['is_logged'])) {
                App::favs($_SESSION['user_id']);
            }
        ?>
    </table>
    </form>
<?php
require 'include/footer.php';
