CREATE DATABASE coil_assistant;

USE coil_assistant;

CREATE TABLE users(
  id int(10) NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(72) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE wires(
  id int(10) NOT NULL AUTO_INCREMENT,
  make VARCHAR(100) NOT NULL,
  awg DECIMAL(3,2) NOT NULL,
  resistivity DECIMAL(6,2) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE coils(
  id INT(10) NOT NULL AUTO_INCREMENT,
  user_id INT(10) NOT NULL,
  wire_id INT(10) NOT NULL,
  length INT(5),
  ohm DECIMAL (4,2),
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (wire_id) REFERENCES wires(id)
);

INSERT INTO wires VALUES(NULL,"Khantal A",0.12,123.00);
INSERT INTO wires VALUES(NULL,"Khantal A",0.16,69.00);
INSERT INTO wires VALUES(NULL,"Khantal A",0.18,54.60);
INSERT INTO wires VALUES(NULL,"Nichel",0.16,15.30);
INSERT INTO wires VALUES(NULL,"Nichel",0.18,11.70);
INSERT INTO wires VALUES(NULL,"Nichel",0.20,9.90);