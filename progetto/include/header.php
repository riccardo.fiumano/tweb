<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
require 'App.php';
?>
<!doctype html>
<html lang="it">
<head>
    <link rel="stylesheet" href="../css/style.css">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= App::$app_name; ?></title>
</head>
<body>
<div class="topnav">
    <a class="active" href="/">Calcolatore</a>
    <?php
    if (isset($_SESSION['is_logged'])) {
        echo '<a id="logout" href="#">Logout</a>';
    } else {
        echo '
            <a href="login.php">Login</a>
    <a href="registrazione.php">Registrazione</a>
        ';
    }
    ?>
</div>
