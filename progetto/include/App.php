<?php

class App
{
    public static $db_host = 'localhost';
    public static $db_user = 'root';
    public static $db_pwd = 'root';
    public static $db_name = 'coil_assistant';

    public static $app_name = 'Coil Assistant';

    public static function dbConnection()
    {
        return new PDO('mysql:host=' . self::$db_host . ';dbname=' . self::$db_name, self::$db_user, self::$db_pwd);
    }

    public static function dropDown()
    {
        $pdo = self::dbConnection()->query("SELECT * FROM wires");
        $pdo->execute();
        $wires = $pdo->fetchAll(PDO::FETCH_ASSOC);
        foreach ($wires as $wire) {
            echo '<option value="'.$wire['awg'].'-'.$wire['resistivity'].'-'.$wire['id'].'">'.$wire['awg'].' mm '.$wire['make'].'</option>';
        }
    }

    public static function favs($user_id) {
        $pdo = self::dbConnection()->prepare("SELECT * FROM coils WHERE user_id = ?");
        $pdo->bindParam(1,$user_id);
        $pdo->execute();
        $favs = $pdo->fetchAll(PDO::FETCH_ASSOC);
        foreach ($favs as $fav) {
            $wire = self::dbConnection()->prepare("SELECT * FROM wires WHERE id = ?");
            $wire->bindParam(1, $fav['wire_id']);
            $wire->execute();
            $wire = $wire->fetch(PDO::FETCH_ASSOC);
            echo '<tr>
            <td class="hide">'.$fav['id'].'</td>
            <td>'.$fav['length'].' mm - </td>
            <td>'.$wire['awg'].' - '.$wire['resistivity'].' - '. $wire['make'].'</td>
            <td><img class="trash" src="../img/icon/trash-alt.svg"></td>
        </tr>';
        }
    }

    public static function register($username, $password) {
        $pdo = self::dbConnection()->prepare("SELECT * FROM users WHERE username = ?");
        $pdo->bindParam(1,$username);
        $pdo->execute();

        if ($pdo->rowCount()>0) {
            echo '{"status":"fail","messaggio":"Esiste già un account con questo username"}';
        } else {
            $reg = self::dbConnection()->prepare("INSERT INTO users VALUES(NULL, ?, ?)");
            $reg->bindParam(1,$username);
            $password = password_hash($password,PASSWORD_DEFAULT);
            $reg->bindParam(2, $password);
            $reg->execute();
            if ($reg->rowCount() !== 0) {
                echo '{"status":"success","messaggio":"La registrazione è andata a buon fine"}';
            } else {
                echo '{"status":"fail","messaggio":"La registrazione non è andata a buon fine"}';
            }
        }
    }

    public static function login($username, $password) {
        $pdo = self::dbConnection()->prepare("SELECT * FROM users WHERE username = ?");
        $pdo->bindParam(1, $username);
        $pdo->execute();
        $pdo = $pdo->fetch(PDO::FETCH_ASSOC);

        if (password_verify($password,$pdo['password'])) {
            session_start();
            $_SESSION['user_id'] = $pdo['id'];
            $_SESSION['username'] = $pdo['username'];
            $_SESSION['is_logged'] = 1;
            echo '{"status":"success","messaggio":"Login eseguito"}';
        } else {
            echo '{"status":"fail","messaggio":"Credenziali errate"}';
        }
    }

    public static function addCoil($user_id,$wire,$lunghezza,$risultato) {
        $pdo = self::dbConnection()->prepare("INSERT INTO coils VALUES(NULL, ?, ?, ?, ?)");
        $pdo->bindParam(1, $user_id);
        $wire = explode('-',$wire);
        $pdo->bindParam(2,$wire[2]);
        $pdo->bindParam(3,$lunghezza);
        $pdo->bindParam(4,$risultato);
        $pdo->execute();
        if ($pdo->rowCount() !== 0) {
            echo '{"status":"success","messaggio":"Preferito aggiunto"}';
        } else {
            echo '{"status":"fail","messaggio":"Non è stato posibile aggiungere un preferito"}';
        }
    }

    public static function rmCoil($id) {
        $pdo = self::dbConnection()->prepare("DELETE FROM coils WHERE id = ?");
        $pdo->bindParam(1,$id);
        $pdo->execute();

        if($pdo->rowCount() !==0) {
            echo '{"status":"success","messaggio":"Preferito rimosso!"}';
        } else {
            echo '{"status":"fail","messaggio":"Non è stato posibile rimuovere un preferito"}';
        }
    }
}